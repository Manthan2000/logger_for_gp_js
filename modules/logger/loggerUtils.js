// import log from "../../models/loggerModelsql";
const express = require('express')
const mongoose = require('mongoose')
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
var util = require('util');
var encoder = new util.TextEncoder('utf-8');
const Event = require("../../model/loggerModel");
const dotenv = require('dotenv')
const path = require('path');
const color = require('cli-color')
dotenv.config({path: path.join(__dirname, '../.env')})
class LoggerUtils {

    async connect(connectionString) {
        try {
             await mongoose.connect(connectionString);
             const data = "info: "+ new Date() +":    "+"Connected to MongoDB"
             console.info(data);
        } catch (error) {
          const data = "error: "+ new Date() +":    "+ color.red("Error connecting to MongoDB:")
          console.info(data, error);
        }
    }

   async activity(event, sub_event, data) {
    try {
      if (process.env.DATABASETYPE === "mysql") {
        return await log.create({
          name: message.name,
          userId: message.userId,
          type: message.type,
          description: message.description,
          metadata: message.metadata,
        });
      }
      if (process.env.DATABASETYPE === "mongodb") {
        console.log("Welcolme to Mongodb!!");
        const eventDoc = {
          event,
          sub_event,
          data,
          // timestamp: new Date()
      };
     
      // Insert the event document into the collection
      const events = new Event(eventDoc);
      await events.save();
      const logMsg = "info: "+ new Date() +":    "+color.yellow("Log info is inserted in "+process.env.DATABASETYPE+" database")
      console.info(logMsg);
      
      }
    } catch (err) {
      console.log(err);
      return err;
    }
  }
}

module.exports = LoggerUtils;