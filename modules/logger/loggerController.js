// import { LoggerUtils } from "../logger/loggerUtils";
const LoggerUtils = require('./loggerUtils')
class LoggerController {

  loggerUtils = new LoggerUtils();

  connect = async (connectionString) => {
    try {
      await this.loggerUtils.connect(connectionString)
    } catch (error) {
      console.log(error);
    }
  }

  activity = async (event, sub_event, data) => {
    try {
      await this.loggerUtils.activity(event, sub_event, data);
    } catch (error) {
      console.log(error);
    }
  };
}

module.exports = LoggerController;