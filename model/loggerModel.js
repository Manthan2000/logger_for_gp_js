const mongoose = require('mongoose');

const eventSchema = mongoose.Schema({
    event:{type:String,required:true},
    sub_event:{type:String,required:true},
    data:{type:Object,required:true},
    expire_at: {type: Date, default: Date.now, expires: 60} //expire records after 1 minutes from db
},
{
    versionKey: false,
    timestamps: true
})

module.exports = mongoose.model("event", eventSchema);
