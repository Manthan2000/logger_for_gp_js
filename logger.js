const express = require('express')
// const mongoose = require('mongoose')
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
var util = require('util');
var encoder = new util.TextEncoder('utf-8');
require('dotenv').config()
const Event = require("../loggerManagement/model/loggerModel");
// console.log(process.env.DATABASETYPE,"TYPE");

// const dbconnection = async () => {
//     await mongoose.connect("mongodb://localhost/User_Management");
//     console.log("Connected!")
// };

// dbconnection();

const mongoose = require('mongoose');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'logger';

// MongoDB Client
// const client = new MongoClient(url, { useUnifiedTopology: true });

// Logger Class
class Logger {
    constructor() {
        this.collection = null;
    }

    async connect(connectionString) {
        try {
            // Connect to the MongoDB server
            //   await mongoose.connect('mongodb://localhost:27017/logger');
            await mongoose.connect(connectionString);


            // Access the logger database
            //   const db = mongoose.db(dbName);

            // Access the events collection
            //   this.collection = db.collection('events');

            console.log('Connected to MongoDB');
        } catch (error) {
            console.error('Error connecting to MongoDB:', error);
        }
    }

    async logEvent(event, subEvent, data) {
        try {
            // Create the event document
            const eventDoc = {
                event,
                subEvent,
                data,
                timestamp: new Date()
            };

            // Insert the event document into the collection
            //   await this.collection.insertOne(eventDoc);
            const events = new Event(eventDoc);
            await events.save();

            console.log('Event logged successfully!');
        } catch (error) {
            console.error('Error logging event:', error);
        }
    }
}
const logger = new Logger();
logger.connect('mongodb://localhost:27017/logger');

logger.logEvent('User', 'Login', { username: 'john123' });
logger.logEvent('Data', 'Processing', { file: 'data.csv' });
