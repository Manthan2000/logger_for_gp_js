const LoggerController = require('./modules/logger/loggerController')

require('dotenv').config()

const loggerController = new LoggerController()
module.exports = loggerController;
// loggerController.connect('mongodb://localhost:27017/logger');
// loggerController.activity('Data', 'Processing', { file: 'data.csv' })
// loggerController.activity('User', 'Login', { username: 'john12' });   